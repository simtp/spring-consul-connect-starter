@file:Suppress("SpringJavaInjectionPointsAutowiringInspection")

package org.simtp.consul.connect.starter

import org.bouncycastle.cert.X509CertificateHolder
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.openssl.PEMKeyPair
import org.bouncycastle.openssl.PEMParser
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.web.server.SslStoreProvider
import org.springframework.boot.web.server.WebServerFactoryCustomizer
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory
import org.springframework.cloud.bootstrap.config.PropertySourceLocator
import org.springframework.cloud.consul.ConsulProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.support.ResourcePropertySource
import org.springframework.web.client.RestTemplate
import java.io.IOException
import java.io.StringReader
import java.security.KeyFactory
import java.security.KeyStore
import java.security.PrivateKey
import java.security.Security
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.security.spec.PKCS8EncodedKeySpec
import java.util.*

/**
 *
 */
@Configuration
@ComponentScan
open class SpringConsulServiceAutoConfiguration {

    /**
     *
     */
    @Configuration
    open class NestConfiguration(@Value("\${spring.application.name}") var applicationName: String,
                                 private val consulProperties: ConsulProperties): WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> {
        private val log = LoggerFactory.getLogger(SpringConsulServiceAutoConfiguration::class.java)

        /**
         *
         */
        override fun customize(factory: ConfigurableServletWebServerFactory) {
            factory.setSslStoreProvider(sslStoreProvider())
        }

        /**
         *
         */
        @Bean
        open fun sslStoreProvider(): SslStoreProvider {
            return object : SslStoreProvider {
                override fun getKeyStore(): KeyStore {
                    return serverKeyStore()
                }

                override fun getTrustStore(): KeyStore {
                    return trustsKeyStore()
                }
            }
        }

        /**
         *
         */
        @Suppress("UNCHECKED_CAST")
        @Bean
        open fun consulServiceTrusts(): List<Pair<String, X509Certificate>> {

            val url = "${consulProperties.scheme ?: "http"}://${consulProperties.host}:${consulProperties.port}/v1/agent/connect/ca/roots"
            try {
                val ops = RestTemplate()
                log.info("URL used for retrieving trusts {}", url)
                return ops.getForEntity(url, Map::class.java)
                    .body
                    ?.let { it["Roots"] as List<Map<String, String>> }
                    ?.filter { it["Active"] as Boolean? ?: error("Active flag is not defined $url") }
                    ?.map { root -> (root["Name"]?.replace(" ", "") ?: error("Failed to retrieve certificate name $url")) to root["RootCert"]}
                    ?.map { it.first to it.second!!.toX509Certificate()!! }
                    ?: emptyList()

            } catch (ex: Exception ) {
                throw IllegalArgumentException("Failed to retrieve trusts $url", ex)
            }
        }

        /**
         *
         */
        @Bean
        open fun consulServiceCerts(): Pair<PrivateKey, List<X509Certificate>> {

            val url = "${consulProperties.scheme ?: "http"}://${consulProperties.host}:${consulProperties.port}/v1/agent/connect/ca/leaf/${applicationName}.consul.home"
            try {
                val ops = RestTemplate()
                log.info("URL used for retrieving server certs {}", url)

                return ops.getForEntity(url, Map::class.java)
                    .body
                    ?.let {
                        val privateKey = it["PrivateKeyPEM"].toString().toPrivateKey()
                        val now = Date()
                        val serverCert = it["CertPEM"].toString().toX509Certificate()!!
                        val rootCert = consulServiceTrusts().first { pair -> pair.second.subjectDN == serverCert.issuerDN
                                && pair.second.notAfter.after(now)
                                && pair.second.notBefore.before(now)
                        }.second
                        privateKey to listOf(serverCert, rootCert)
                    }
                    ?: throw IllegalStateException("Could not retrieve server certificate $url")
            } catch (ex: Exception ) {
                throw IllegalStateException("Could not retrieve server certificate $url", ex)
            }
        }

        /**
         *
         */
        @Bean
        open fun trustsKeyStore(): KeyStore {

            val keyStore = KeyStore.getInstance("JKS")
            keyStore.load(null, null)
            consulServiceTrusts()
                .forEach {
                    if (keyStore.getCertificateAlias(it.second) == null) {
                        log.info("Key store does not already contain certificate")
                        keyStore.setCertificateEntry(it.first, it.second)
                    } else {
                        log.info("Key store already contains certificate")
                    }
                }
            return keyStore
        }

        /**
         *
         */
        @Bean
        open fun serverKeyStore(): KeyStore {

            val keyStore = KeyStore.getInstance("PKCS12")
            keyStore.load(null, null)
            consulServiceCerts()
                .apply {
                    keyStore.setKeyEntry(applicationName, this.first, null, this.second.toTypedArray())
                }
            return keyStore
        }

    }



}
private val keyFactory = KeyFactory.getInstance("EC")

/**
 *
 */
fun String.toPrivateKey(): PrivateKey {
    val pemParser = PEMParser(StringReader(this))
    val bcKeyPair = pemParser.readObject() as PEMKeyPair
    val keySpec = PKCS8EncodedKeySpec(bcKeyPair.privateKeyInfo.encoded)
    return keyFactory.generatePrivate(keySpec)
}

/**
 *
 */
@Throws(CertificateException::class, IOException::class)
fun String.toX509Certificate(): X509Certificate? {
    Security.addProvider(BouncyCastleProvider())
    val reader = StringReader(this)
    val parser = PEMParser(reader)
    val certificateHolder = parser.readObject() as X509CertificateHolder
    reader.close()
    return JcaX509CertificateConverter().setProvider( "BC" )
        .getCertificate( certificateHolder )
}

/**
 *
 */
class ConsulConnectPropertySourceLocator: PropertySourceLocator {

    override fun locate(environment: Environment?): org.springframework.core.env.PropertySource<*> {
        println("Loading resources")
        return ResourcePropertySource("ConsulConnect", ClassPathResource("consul-connect.properties"))
    }

}

