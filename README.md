# Spring Consul Connect Starter

## Note this library is still under development and is not ready for production environments

## Overivew

This library provides native integration with Hashicorp Consul Connect for Spring Boot
applications.

## Usage
```xml
<dependency>
    <groupId>org.simtp</groupId>
    <artifactId>spring-consul-connect-starter</artifactId>
    <version>${project.version}</version>
</dependency>
```

